package com.example.MyBookShopApp.dto;


public class BookDTO {
     private String title;
     private Integer priceOld;
     private Integer price;
     private String authorName;

    public BookDTO(String title, Integer priceOld, Integer price, String authorName) {
        this.title = title;
        this.priceOld = priceOld;
        this.price = price;
        this.authorName = authorName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(Integer priceOld) {
        this.priceOld = priceOld;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
