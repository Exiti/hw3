package com.example.MyBookShopApp.data;

import com.example.MyBookShopApp.dto.BookDTO;
import com.example.MyBookShopApp.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;

import java.util.List;

@Service
public class BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }



    @Transactional
    public List<BookDTO> getBooksData(){
        List<Book> books = bookRepository.findAll();
        //System.out.println(bookRepository.findAllById(Collections.singleton(1)));
        List<BookDTO> bookDTO = new ArrayList<>();
        for (Book book : books) {
            bookDTO.add(new BookDTO(book.getTitle(), book.getPriceOld(), book.getPrice(), book.getAuthor().getName()));
        }
        System.out.println("----------------");
        return bookDTO;
    }
}
