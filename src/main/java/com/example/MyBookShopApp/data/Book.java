package com.example.MyBookShopApp.data;




import javax.persistence.*;

@Entity
@Table(name = "books")
public class Book {

    @Id
    @Column(name="id")
    private Integer id;
    @ManyToOne
    @JoinColumn(name="author_id", referencedColumnName = "id")
    private Author author;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Column(name = "title")
    private String title;
    @Column(name="price_old")
    private Integer priceOld;
    @Column(name="price")
    private Integer price;


    public Book() {

    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", priceOld=" + priceOld +
                ", price=" + price +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(Integer priceOld) {
        this.priceOld = priceOld;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
