DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS authors;
CREATE TABLE authors (
                         id INT AUTO_INCREMENT PRIMARY KEY,
                         name VARCHAR(100)
);
CREATE TABLE  books(
                       id INT  PRIMARY KEY,
                       price INT,
                       price_old  INT,
                       title VARCHAR(250) NOT NULL,
                       author_id INT,
                       FOREIGN KEY (author_id) REFERENCES authors(id)
);
